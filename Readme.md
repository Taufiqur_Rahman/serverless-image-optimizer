# Create lambda function by invoking S3 bucket locally

### Description:
***
Creating a lambda function locally by node.js and invoke into AWS S3 Bucket. Find the bucket in S3 which has been created through local enviroment. Create an **"originals"** folder in it and inside originals folder,create a folder with the size encoded in the name (e.g. 200x200). After that resized/optimized picture will be stored in **"Procceds"** folder.

### Prerequisites:
***
* Environment: Windows.

* [Node.js](https://nodejs.org/en/) version: v8.11.1 or latest.

* NPM version: latest version. If not install,then-
```
$ npm i latest-version
```
* Docker installed in local mechine.
* AWS SAM CLI install

### To-do:
***
* Upload a png in originals folder in S3 bucket.
* Create a bucket through AWS SAM CLI.

### Configuration:
***
**Step 1:** 
* Install the serverless framework
```
$ npm install serverless -g
```
```
$ mkdir serverless-image-optimizer
```
```
$ cd serverless-image-optimizer
```
```
$ sls create --template aws-nodejs
```

* After running the following commands there should be three files: 
.gitignore
handler.js
serverless.yml

**Step 2:** 
Check the **serverless.yml** file and copy & paste it to your **serverless.yml**.

{For addtional information,check **Addtional information about lambda fucntion** in bottom.}



**Step 3:**
*  we have **handler.js**, rename it **imageOptimizer.js** .Create a lambda fucntion. (check the **imageOptimizer.js**). 

* Creating a package.json and adding following dependencies.
```
yarn init -y
yarn add gm
yarn add imagemin
yarn add imagemin-pngquant
```
* If yarn is not installed, then write the following command in command line.
```
npm install yarn- g
```
**Step:4**

* Start the docker.

* To deploy run:
```
sls deploy
```
* After that if the command line showing sls Conflicts with Powershell sls (Select-string) like:
```
 cmdlet Select-String at Comamnd Pipeline Position 1
 Supply values for the following parameters:
 Path[0]:_
``` 
 Then you need to remove the PowerShell sls alias is by running the following in PowerShell:
 ```
 Remove-Item alias:sls
 ```
 **Now, go to S3 in AWS console, find your newly created bucket, create an originals folder in it, create a folder with the size encoded in the name (for example 200x200), add an image to that folder then you should see a resized/optimized image in the processed folder. Processed folder need to create in newly created bucket.**
 
 ### Addtional configuration:
***
- To check the S3 buckets in console through aws sam
```
aws s3 ls 
```
- To check the bucket what it have inside
```
aws s3 ls {Bucket-Name}
```
- To check the sub-folder's files in S3 bucket
```
aws s3 ls {Bucket-Name}/{Folder-Name}/
```
- To permanently remove the sls PowerShell alias, you can change **Microsoft.PowerShell_profile.ps1 file.**
From PowerShell open your profile page in Notepad with the following command:
```
notepad $profile
```
- Add the following to the file and save.
```
remove-item alias:sls
```
- The profile can be reloaded by running the following from Powershell
```
. $profile
```
### Additional Knowledge about lambda fucntion
***
Go to the **serverless.yml** and replace the bucket name
(e.g.
```
Resource: 'arn:aws:s3:::image-optimizer-images'
```
)

* There are the important part of the code is:
```
iamRoleStatements:
    - Effect: Allow
      Action:
        - s3:PutObject
        - s3:GetObject
      Resource: 'arn:aws:s3:::image-optimizer-images'
```
* Then give the lambda get and put access to the bucket:
```
imageOptimizer:
    handler: imageOptimizer.handler
```
* Define the lambda fucntion name and location
```
environment:
      BUCKET: ${image-optimizer-images}
```
* Sets the S3 bucket name in an environment variable
```
events:
      - s3:
        bucket: ${image-optimizer-images}
        event: s3:ObjectCreated:*
        rules:
          - prefix: originals          
          - suffix: .png
```
* Sets an event in the S3 bucket that is going to call the lambda every time a png image is added to the originals folder.
```
timeout: 12
```

